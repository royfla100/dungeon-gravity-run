extends ParallaxLayer

export(float) var SPEED = -5
func _process(delta):
	self.motion_offset.x += SPEED
