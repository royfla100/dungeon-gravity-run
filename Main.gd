extends Node2D
var sections = [
	preload("res://Sections/0.tscn"),
	preload("res://Sections/1.tscn"),
	preload("res://Sections/2.tscn"),
	preload("res://Sections/3.tscn"),
	preload("res://Sections/4.tscn"),
	preload("res://Sections/5.tscn"),
	preload("res://Sections/6.tscn")
	
] #Add any layouts of the dungeon here. make sure their start and end are roughly the same as each other

export var speed:int=175 #Movement Speed
var deathScreen:bool=false
var score:int = 0
var highscore:int = 0
onready var respawnTimer = $Respawn
onready var scoreTimer = $ScoreTimer
onready var gameOver = get_node("HUD/GameOver")
onready var pauseButton = get_node("HUD/PauseButton")
const savePath = "user://statistics.sav"

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	$HUD.update_score(score)
	load_score()
	$HUD.place_high(highscore)
	scoreTimer.start()
	spawn_start(0, 0)
	spawn_inst(1024, 0)
	Global.gemCount = 0

func _physics_process(delta):
	for area in $Sections.get_children():
		area.position.x -= speed*delta
		if area.position.x < -1088:
			spawn_inst(area.position.x+2048, 0)
			area.queue_free()
		if respawnTimer.is_stopped() and deathScreen:
			gameOver.show()
	
func spawn_start(x, y):
	var start = sections[0].instance()
	start.position= Vector2(x, y)
	$Sections.add_child(start)
	$music.play()
func spawn_inst(x, y):
	var inst = sections[randi() % len(sections)].instance()
	inst.position= Vector2(x, y)
	$Sections.add_child(inst)
	
func _on_ScoreTimer_timeout():
	score += 1
	$HUD.update_score(score)

	
func _on_Player_dead():
	$music.stop()
	$deathSound.play()
	pauseButton.hide()
	get_node("HUD/TextureRect").hide()
	if score > highscore:
		highscore = score
		save_score()
		$HUD.place_high(highscore)
	deathScreen = true
	speed = 0
	$ParallaxBackground/Background.SPEED = 0
	$ParallaxBackground/BackgroundL1.SPEED = 0
	$ParallaxBackground/BackgroundL2.SPEED = 0
	scoreTimer.stop()
	respawnTimer.start()
	
func save_score():
	var saveFile = File.new() 
	saveFile.open(savePath, File.WRITE)
	saveFile.store_var(highscore)
	saveFile.close()
func load_score():
	var saveFile = File.new()
	saveFile.open(savePath, File.READ)
	highscore = saveFile.get_var()
	saveFile.close()

