extends Area2D
signal gem

func _on_Gem_body_entered(body: Node):
	Global.update_gems(1)
	emit_signal("gem")
	queue_free()
