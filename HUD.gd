extends CanvasLayer



onready var endScore = get_node("GameOver/CenterContainer/VBoxContainer/ThisScore")
onready var endHiScore = get_node("GameOver/CenterContainer/VBoxContainer/ThisScoreHi")
onready var pauseScore = get_node("Pause/CenterContainer/VBoxContainer/ThisScore")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
func place_high(highscore):
	var hiScoring = "%07d" %[highscore]
	$HiScoreLabel.text = "TOP:" + hiScoring
	endHiScore.text = hiScoring
func update_score(score):
	var scoring = "%07d" %[score]
	$ScoreLabel.text = scoring
	endScore.text = scoring
	pauseScore.text = scoring
	var gems = "%02d" %[Global.gemCount]
	$GemLabel.text = gems
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

	
func _on_Player_dead():
	pass # Replace with function body.
