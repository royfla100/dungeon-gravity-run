extends KinematicBody2D
signal dead



export var gravity:int=600
var currentGravity:int=gravity
export var flipped:bool=false
export var grounded:bool=true
export var dead:bool=false
var velocity:Vector2=Vector2()
onready var jumpTimer = $JumpBufferTimer

func _physics_process(delta):
	$AnimatedSprite.play() #Animate the sprites
	velocity.y += currentGravity * delta
	velocity.y=move_and_slide(velocity,Vector2.UP).y
	
	if is_on_ceiling() or is_on_floor():
		grounded = true
		if !dead: $AnimatedSprite.animation = "Running"
	else:
		grounded = false
		if !dead: $AnimatedSprite.animation = "Falling"
		
	if Input.is_action_just_pressed("Croc"):
		jumpTimer.start()
	if grounded:
		if !jumpTimer.is_stopped():
			gravJump()
	#flipped handler
	if flipped:
		currentGravity = gravity
		$AnimatedSprite.flip_v = false
	else:
		currentGravity = gravity * -1
		$AnimatedSprite.flip_v = true
func gravJump():
		flipped = !flipped	
		
func die():
	emit_signal("dead")
	dead = true
	flipped = true
	$GoldSpray.emitting = true;
	$CollisionShape2D.set_deferred("disabled", true)
	$AnimatedSprite.animation = "Dead"
	$AnimatedSprite.stop()
	
	

func _on_VisibilityNotifier2D_screen_exited():
	die()
